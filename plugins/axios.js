import axios from 'axios'
import {getBaseUrl} from '@/utils/index'

import {createPinia} from "pinia"
import {createApp} from "vue"

const pinia = createPinia()
const app = createApp()
app.use(pinia)

import {useGlobalStore} from '@/stores/global'

const store = useGlobalStore()
export default defineNuxtPlugin((nuxtApp) => {
    let api = axios.create({
        baseUrl: getBaseUrl(),
        withCredentials: true,
        headers: {
            'Accept': 'application/json',
            'content-type': 'application/json',
            // 'Access-Control-Allow-Credentials': '*',
            // 'Access-Control-Allow-Origin': '*',
            // 'origin': config.public.baseUrl,
            // 'Cache-Control': 'no-cache',

            // 'Access-Control-Allow-Headers ': "Content-Type"
            'mode': 'no-cors'
        }
    })


// response interceptor
    api.interceptors.response.use(undefined,
        error => {
            if (error.response.status === 403) {

                store.logout()
                localStorage.clear()

            }

            return Promise.reject(error)
        }
    )
    api.interceptors.request.use(
        config => {
            config.baseURL = getBaseUrl()

            let token = ''
            if (JSON.parse(localStorage.getItem('userInfo'))) {
                token = JSON.parse(localStorage.getItem('userInfo')).token
            } else if (JSON.parse(localStorage.getItem('adminInfo'))) {
                token = JSON.parse(localStorage.getItem('adminInfo')).token
            }

            if (token) {
                config.headers.Authorization = `Bearer ${token}`
            }

            return config
        },
        error => Promise.reject(error)
    )

    return {
        provide: {
            api: api
        }
    }
})