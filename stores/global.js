// use pinia technology as a state management
import {defineStore} from "pinia";

export const useGlobalStore = defineStore('global', {
    state: () => {
        return {
            isMobile: false,
            insuranceCategories: [],
            drawerIsOpen: false,
            selected_user_insurance: {},
            selected_admin_insurance: {},

            full_loading: false,

            device: '',
            displayToast: false
        }
    },
    actions: {
        // show or hide full loading component
        setFullLoading(value) {
            this.full_loading = value
        },
        // set isMobile flag for mobile device
        setIsMobile(value) {
            this.isMobile = value
        },
        // set device type(xs,sm,...) for devices. todo: clear isMobile & just use device to optimize code
        setDevice(value) {
            this.device = value
        },
        // get insurances categories for admin or user & set it into localstorage
        getInsuranceCategories(role) {

            if (role === 'user') {

                if (JSON.parse(localStorage.getItem('user_insurance_categories'))) {
                    this.insuranceCategories = JSON.parse(localStorage.getItem('user_insurance_categories'))
                } else {
                    this.fetchInsuranceCategories(role, '/api/user/getActiveCategories')
                }

            } else if (role === 'admin') {

                if (JSON.parse(localStorage.getItem('admin_insurance_categories'))) {
                    this.insuranceCategories = JSON.parse(localStorage.getItem('admin_insurance_categories'))
                } else {
                    this.fetchInsuranceCategories(role, '/api/admin/faqs/getActiveCategories')
                }

            }

        },
        fetchInsuranceCategories(role, fetchUrl) {
            this.full_loading = true

            setTimeout(() => {
                this.full_loading = false
                this.insuranceCategories = [
                    {
                        "id": "644e45994f827032079267b1",
                        "categoryLabel": "life_insurance",
                        "categoryPersianLabel": "بیمه عمر",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:40:25.644Z"
                    },
                    {
                        "id": "644e46f34f827032079267b5",
                        "categoryLabel": "car_insurance",
                        "categoryPersianLabel": "بیمه اتومبیل",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:46:11.593Z"
                    },
                    {
                        "id": "644e470b4f827032079267b9",
                        "categoryLabel": "portage_insurance",
                        "categoryPersianLabel": "بیمه باربری",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:46:35.950Z"
                    },
                    {
                        "id": "644e47234f827032079267bd",
                        "categoryLabel": "shipping_insurance",
                        "categoryPersianLabel": "بیمه حمل و نقل",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:46:59.496Z"
                    },
                    {
                        "id": "644e47394f827032079267c1",
                        "categoryLabel": "incidents_insurance",
                        "categoryPersianLabel": "بیمه حوادث",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:47:21.477Z"
                    },
                    {
                        "id": "644e474e4f827032079267c5",
                        "categoryLabel": "health_insurance",
                        "categoryPersianLabel": "بیمه درمان",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:47:42.898Z"
                    },
                    {
                        "id": "644e47614f827032079267c9",
                        "categoryLabel": "travel_insurance",
                        "categoryPersianLabel": "بیمه مسافرتی",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:48:01.802Z"
                    },
                    {
                        "id": "644e47744f827032079267cd",
                        "categoryLabel": "liability_insurance",
                        "categoryPersianLabel": "بیمه مسئولیت",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:48:20.495Z"
                    },
                    {
                        "id": "644e478c4f827032079267d1",
                        "categoryLabel": "motorcycle_insurance",
                        "categoryPersianLabel": "بیمه موتور سیکلت",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:48:44.192Z"
                    },
                    {
                        "id": "644e47ad4f827032079267d5",
                        "categoryLabel": "electronic_equipment_insurance",
                        "categoryPersianLabel": "بیمه تحهیزات الکترونیکی",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:49:17.071Z"
                    },
                    {
                        "id": "644e47c84f827032079267d9",
                        "categoryLabel": "parsian_specific_questions",
                        "categoryPersianLabel": "سوالات اختصاصی پارسیان",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:49:44.860Z"
                    },
                    {
                        "id": "644e48064f827032079267dd",
                        "categoryLabel": "fire_insurance",
                        "categoryPersianLabel": "بیمه آتش سوزی",
                        "isActive": true,
                        "createdAt": "2023-04-30T10:50:46.333Z"
                    }
                ]
                if (role === 'user') {
                    localStorage.setItem('user_insurance_categories', JSON.stringify(this.insuranceCategories))
                } else if (role === 'admin') {
                    localStorage.setItem('admin_insurance_categories', JSON.stringify(this.insuranceCategories))
                }
            }, 3000)

            // useNuxtApp().$api.get(fetchUrl)
            //     .then(response => {
            //         this.insuranceCategories = response.data.data
            //         this.full_loading = false
            //         if (role === 'user') {
            //             localStorage.setItem('user_insurance_categories', JSON.stringify(this.insuranceCategories))
            //         } else if (role === 'admin') {
            //             localStorage.setItem('admin_insurance_categories', JSON.stringify(this.insuranceCategories))
            //         }
            //
            //
            //     }).catch(error => {
            //
            //     console.log(error, ' error')
            //     this.full_loading = false
            // })
        },
        // toggle navigation bar in mobile
        toggleDrawer() {
            this.drawerIsOpen = !this.drawerIsOpen
        },
        // close navigation bar in mobile
        closeDrawer() {
            this.drawerIsOpen = false
        },
        // call logout api to expire user or admin token
        expireToken() {
            const router = useRouter()
            const route = useRoute()
            this.full_loading = true
            setTimeout(()=>{
                if (route.path.includes('/user')) {
                    router.push({
                        path: `/user/login`
                    })
                } else {
                    router.push({
                        path: `/admin/login`
                    })
                }

                this.full_loading = false
                localStorage.clear()
                this.closeDrawer()
            },3000)


            // useNuxtApp().$api.get(`/api/auth/logout`)
            //     .then(response => {
            //         if (response.data.success) {
            // redirect to proper login page after token expired

            //         }
            //     }).catch(error => {
            //     //
            //     console.log(error, ' error')
            //     this.full_loading = false
            //
            // })
        },
        logout() {
            const router = useRouter()
            const route = useRoute()
            // if local storage is clear, there is nothing to expire. so we just redirect user to login
            if (route.path.includes('/user') && !JSON.parse(localStorage.getItem('userInfo'))) {
                router.push({
                    path: `/user/login`
                })
            } else if (route.path.includes('/admin') && !JSON.parse(localStorage.getItem('adminInfo'))) {
                router.push({
                    path: `/admin/login`
                })
            } else {
                this.expireToken()
            }
        },
        toastTimeOut() {
            this.displayToast = true

            setTimeout(() => {
                this.displayToast = false
            }, 3000);
        },
        setMobileNavigationHeight(value) {
            this.mobileNavigationHeight = value
            localStorage.setItem('mobileNavigationHeight', this.mobileNavigationHeight)
        },
        setSelectedCategoryHeight(value) {
            this.selectedCategoryHeight = value
            localStorage.setItem('selectedCategoryHeight', this.selectedCategoryHeight)
        },
        setSuggestedQuestionsHeight(value) {
            this.suggestedQuestionsHeight = value
            localStorage.setItem('suggestedQuestionsHeight', this.suggestedQuestionsHeight)
        },
        navigateToDashboard() {
            const router = useRouter()
            const route = useRoute()

            if (route.path.includes('/user') && JSON.parse(localStorage.getItem('userInfo'))) {
                router.push({
                    path: `/${JSON.parse(localStorage.getItem('userInfo')).role}/dashboard`
                })
            } else if (route.path.includes('/admin') && JSON.parse(localStorage.getItem('adminInfo'))) {
                router.push({
                    path: `/${JSON.parse(localStorage.getItem('adminInfo')).role}/dashboard`
                })
            } else if (route.path.includes('/developer') && JSON.parse(localStorage.getItem('developerInfo'))) {
                router.push({
                    path: `/${JSON.parse(localStorage.getItem('developerInfo')).role}/dashboard`
                })
            }
        },

        navigateToProfile() {
            const router = useRouter()
            const route = useRoute()

            if (route.path.includes('/user') && JSON.parse(localStorage.getItem('userInfo'))) {
                router.push({
                    path: `/${JSON.parse(localStorage.getItem('userInfo')).role}/profile`
                })
            } else if (route.path.includes('/admin') && JSON.parse(localStorage.getItem('adminInfo'))) {
                router.push({
                    path: `/${JSON.parse(localStorage.getItem('adminInfo')).role}/profile`
                })
            } else if (route.path.includes('/developer') && JSON.parse(localStorage.getItem('developerInfo'))) {
                router.push({
                    path: `/${JSON.parse(localStorage.getItem('developerInfo')).role}/profile`
                })
            }
        },

        navigateToNoAnswers() {
            const router = useRouter()
            const route = useRoute()

            if (route.path.includes('/admin') && JSON.parse(localStorage.getItem('adminInfo'))) {
                router.push({
                    path: `/${JSON.parse(localStorage.getItem('adminInfo')).role}/no-answers`
                })
            }
        },
    },
})
