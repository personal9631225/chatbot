export default defineNuxtConfig({
    plugins: [],
    modules: [
        '@nuxtjs/tailwindcss',
        '@pinia/nuxt',
    ],

    cache: false,
    ssr: false,
    css: ['~/assets/css/main.scss'],
    tailwindcss: {
        viewer: true
    },

    runtimeConfig: {
        public: {
            baseUrl: process.env.baseUrl,
        }
    },
    vite: {
        define: {
            BASE_URL: JSON.stringify(process.env.VUE_APP_BASE_API)
        }
    }

})
