// const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
    content: ["@/pages/**/*.{js,ts,jsx,tsx,vue,html}", "@/components/**/*.{js,ts,jsx,tsx,vue,html}"],
    theme: {

        screens: {

            'xs': '390px',
            // => @media (min-width: 500px) { ... }

            'sm': '640px', // => @media (min-width: 640px) { ... }

            'md': '768px', // => @media (min-width: 768px) { ... }

            'lg': '1024px',

            // => @media (min-width: 1024px) { ... }

            'lg2': '1023px', 'lg3': '1150px',


            'xl': '1280px',

            'xl2': '1440px', // desktop
            // => @media (min-width: 1280px) { ... }

            '2xl': '1536px', // large desktop
            // => @media (min-width: 1536px) { ... }

            'hd': '1921px', // HD
            // => @media (min-width: 1920px) { ... }

            '2k': '2560px', // 2K
            // => @media (min-width: 2560px) { ... }

            '4k': '3840px', // 4k
            // => @med    ia (min-width: 3840px) { ... }
        },
        extend: {
            // keyframes: {
            //     wiggle: {
            //         '0%, 100%': { transform: 'rotate(-3deg)' },
            //         '50%': { transform: 'rotate(3deg)' },
            //     }
            // },
            // animation: {
            //     wiggle: 'wiggle 1s ease-in-out infinite',
            // },
            // theme: {
            //     screens: {
            //         'xs': '390px',
            //         ...defaultTheme.screens,
            //     },
            // },
            colors: {
                'primary': {
                    200: '#F6F8FF',
                    // 200: '#70C4EF',
                    // 300: '#42B4EC',
                    // 400: '#13A4E9',
                    // 500: '#0A8EDC',
                    // 600: '#0077CE',
                    700: '#2131BA',
                },
                'secondary': {
                    50: '#FFF4DC',
                    100: '#FFDC94',
                    200: '#FCC553',
                    300: '#FBB932',
                    400: '#F9AD11',
                    500: '#EB9C0D',
                    600: '#DC8A09',
                    700: '#BF6700',
                },
                'black': {
                    100: '#222222',
                    60: '#22222299',
                    '60-icon': '#7A7A7A',
                    30: '#2222224d',
                    10: '#2222221a',
                },
                'white': {
                    100: '#FFFFFF',
                    70: '#ffffffb3',
                    30: '#ffffff4d',
                },
                'success': {
                    100: '#BFF0BE',
                    200: '#78D876',
                    300: '#54CC52',
                    400: '#30C02D',
                    500: '#30C02D',
                    600: '#037C00',
                    700: '#035101',
                },
                'error': {
                    100: '#FFC6C6',
                    200: '#F89595',
                    300: '#F16363',
                    400: '#DC2E2E',
                    500: '#BC0000',
                    600: '#950000',
                    700: '#680000',
                },
                'warning': {
                    100: '#EDD599',
                    200: '#D7B04D',
                    300: '#CC9D27',
                    400: '#C08A00',
                    500: '#AE7D00',
                    600: '#9C7000',
                    700: '#644800',
                },
                'information': {
                    100: '#D7E7FF',
                    200: '#77ADFF',
                    300: '#4790FF',
                    400: '#1673FF',
                    500: '#0B59CF',
                    600: '#003F9E',
                    700: '#002B6B',
                },
            },
        }
    }

}