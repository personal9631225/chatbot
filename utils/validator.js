const list = [
    'password',
    'email',
    'mobile',
    'otp',
    'parseArabic',
    'noRule'
]
const email = (value,require , callback) => {

    if(!value && require){
        return callback('وارد کردن ایمیل اجباری است',null)

    }
    if(!value && !require){
        return callback(null,'empty')
    }
    if(value){
        const m = String(value)
            .toLowerCase()
            .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )

        if(!m){
            return callback('ایمیل وارد شده معتبر نیست.',null)
        }
        callback(null,value)
    }

};
const mobile =  (value,require,callback) => {

    if(!value && require){
        return callback('وارد کردن شماره موبایل اجباری است',null)
    }
    if(!value && !require){
        return callback(null,'empty')
    }
    if(value){
        let m = /^(\+98|98|0098|0)?(9\d{2})(\d{7})$/.exec(parseArabic(value))
        /*let operator = ''*/
        let fullnumber = ''
        /*    let mtnNumbers = ['930','931','932','933','934','935','936','937','938','939','901','902','903','904','905','906','907','908']
            let rightelNumbers = ['920','921','922','923','924','925','926','927','928','929']*/
        if (!m) {
            return callback('شماره موبایل وارد شده صحیح نمی باشد',null)
        } else {
            fullnumber =  '0' + m[2] + m[3]
            return callback(null,fullnumber)
        }
    }



}
const parseArabic = (str)=> {
    return Number( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
        return d.charCodeAt(0) - 1632; // Convert Arabic numbers
    }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
        return d.charCodeAt(0) - 1776; // Convert Persian numbers
    }) );
}


const otp = (pin ,required, callback)=>{
    let m = /^(\d{4,5})$/.exec(parseArabic(pin))
    if (!m) {
        callback('کد وارد شده صحیح نمی باشد',null)
    } else {
        callback(null,m[1])
    }

}

const password = (value,require ,callback)=>{
    if(!value && require){
        return callback('وارد کردن رمز عبور اجباری است',null)

    }
    if(!value && !require){
        return callback(null,'empty')
    }
    if(value.length < 5 ){
        return callback('رمز عبور نمیتواند کمتر از 5 کاراکتر باشد',null)
    }
    return callback(null,value)
}

const name = (value,require ,callback)=>{
    if(!value && require){
        return callback('وارد کردن نام اجباری است',null)
    }
    if(!value && !require){
        return callback(null,'empty')
    }
    if(value.length <= 2 ){
        return callback('نام و نام خانوادگی نمیتواند کمتر از 2 کاراکتر باشد',null)
    }
    if(value.length > 70 ){
        return callback('نام و نام خانوادگی نمیتواند بیشتر از 70 کاراکتر باشد',null)
    }
    return callback(null,value)
}
const noRule = (value,require ,callback)=>{

    return callback(null,value)
}




export {
    password,
    email,
    mobile,
    name,
    otp,
    noRule,
    parseArabic,
    list
}
