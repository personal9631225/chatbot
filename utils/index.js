export function getBaseUrl() {
    // const config = useRuntimeConfig();
    return BASE_URL
}

export function groupItems(array, property) {
    return array.reduce(function (groups, item) {
        let name = item[property]
        let group = groups[name] || (groups[name] = []);
        group.push(item);
        return groups;
    }, {});
}