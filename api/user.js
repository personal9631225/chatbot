import request from '@/plugins/axios'

export function login(data) {
    return request({
        url: `/api/auth/getSecurityCode/`,
        method: 'post',
        data
    })
}